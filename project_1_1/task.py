# Создать приложение, генерирующую пароль от 8 до 32 символов, который включает буквы, цифры и спец символы.
# Приложение принимает аргумент длины пароля.

import sys
import random as rnd


length = int(sys.argv[1])
pwd = ''
alph = '!@#$%&*?'
file = open("target/result.txt", "w+")

if 8 <= length <= 32: 
    for _ in range(length):
        temp = rnd.randint(1, 3)
        if temp == 1:
            temp = rnd.randint(1, 2)
            if temp == 1:
                pwd += chr(rnd.randint(65, 90))
            else:
                pwd += chr(rnd.randint(97, 122))
        elif temp == 2:
            pwd += chr(rnd.randint(48, 57))
        elif temp == 3:
            pwd += alph[rnd.randint(0, 7)]
    file.write(pwd)
else:
    file.write('Допустимая длина от 8 до 32 символов!')

file.close()